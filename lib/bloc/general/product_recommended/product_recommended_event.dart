import 'package:meta/meta.dart';

@immutable
abstract class ProductRecommendedEvent {}

class GetProductRecommended extends ProductRecommendedEvent {
  final String sessionId;
  final apiToken;
  GetProductRecommended({@required this.sessionId, @required this.apiToken});
}
