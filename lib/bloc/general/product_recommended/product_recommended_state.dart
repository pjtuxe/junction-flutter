import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProductRecommendedState {}

class InitialProductRecommendedState extends ProductRecommendedState {}

class ProductRecommendedError extends ProductRecommendedState {
  final String errorMessage;

  ProductRecommendedError({this.errorMessage});
}

class ProductRecommendedWaiting extends ProductRecommendedState {}

class GetProductRecommendedSuccess extends ProductRecommendedState {
  final List<ProductModel> productsData;
  GetProductRecommendedSuccess({@required this.productsData});
}
