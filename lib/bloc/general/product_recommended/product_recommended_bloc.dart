import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:ijshopflutter/network/api_provider.dart';
import './bloc.dart';

class ProductRecommendedBloc
    extends Bloc<ProductRecommendedEvent, ProductRecommendedState> {
  ProductRecommendedBloc() : super(InitialProductRecommendedState());

  @override
  Stream<ProductRecommendedState> mapEventToState(
      ProductRecommendedEvent event) async* {
    if (event is GetProductRecommended) {
      yield* _getProductRecommended(event.sessionId, event.apiToken);
    }
  }
}

Stream<ProductRecommendedState> _getProductRecommended(
    String sessionId, apiToken) async* {
  ApiProvider _apiProvider = ApiProvider();

  yield ProductRecommendedWaiting();

  try {
    List<ProductModel> data =
        await _apiProvider.getProductRecommended(sessionId, apiToken);
    yield GetProductRecommendedSuccess(productsData: data);
  } catch (ex) {
    print(ex);
    if (ex.message != 'cancel') {
      yield ProductRecommendedError(errorMessage: ex.message.toString());
    }
  }
}
