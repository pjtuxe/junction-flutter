import 'package:meta/meta.dart';

@immutable
abstract class ProductsEvent {}

class GetProducts extends ProductsEvent {
  final String sessionId;
  final apiToken;
  GetProducts({@required this.sessionId, @required this.apiToken});
}
