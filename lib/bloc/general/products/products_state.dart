import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProductsState {}

class InitialProductsState extends ProductsState {}

class ProductsError extends ProductsState {
  final String errorMessage;

  ProductsError({this.errorMessage});
}

class ProductsWaiting extends ProductsState {}

class GetProductsSuccess extends ProductsState {
  final List<ProductModel> productsData;
  GetProductsSuccess({@required this.productsData});
}
