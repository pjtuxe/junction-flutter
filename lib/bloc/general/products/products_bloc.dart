import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:ijshopflutter/network/api_provider.dart';
import './bloc.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  ProductsBloc() : super(InitialProductsState());

  @override
  Stream<ProductsState> mapEventToState(ProductsEvent event) async* {
    if (event is GetProducts) {
      yield* _getProducts(event.sessionId, event.apiToken);
    }
  }
}

Stream<ProductsState> _getProducts(String sessionId, apiToken) async* {
  ApiProvider _apiProvider = ApiProvider();

  yield ProductsWaiting();

  try {
    List<ProductModel> data =
        await _apiProvider.getProducts(sessionId, apiToken);
    yield GetProductsSuccess(productsData: data);
  } catch (ex) {
    print(ex);
    if (ex.message != 'cancel') {
      yield ProductsError(errorMessage: ex.message.toString());
    }
  }
}
