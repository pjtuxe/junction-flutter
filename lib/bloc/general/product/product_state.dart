import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProductState {}

class InitialProductState extends ProductState {}

class ProductError extends ProductState {
  final String errorMessage;

  ProductError({this.errorMessage});
}

class ProductWaiting extends ProductState {}

class GetProductSuccess extends ProductState {
  final ProductModel productData;
  GetProductSuccess({@required this.productData});
}
