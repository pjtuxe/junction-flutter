import 'package:meta/meta.dart';

@immutable
abstract class ProductEvent {}

class GetProduct extends ProductEvent {
  final String sessionId;
  final apiToken;
  final String productId;
  GetProduct(
      {@required this.productId,
      @required this.sessionId,
      @required this.apiToken});
}
