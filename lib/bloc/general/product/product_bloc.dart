import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:ijshopflutter/network/api_provider.dart';
import './bloc.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc() : super(InitialProductState());

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is GetProduct) {
      yield* _getProduct(event.productId, event.sessionId, event.apiToken);
    }
  }
}

Stream<ProductState> _getProduct(
    String productId, String sessionId, apiToken) async* {
  ApiProvider _apiProvider = ApiProvider();

  yield ProductWaiting();

  try {
    ProductModel data =
        await _apiProvider.getProduct(productId, sessionId, apiToken);
    yield GetProductSuccess(productData: data);
  } catch (ex) {
    if (ex.message != 'cancel') {
      yield ProductError(errorMessage: ex.message.toString());
    }
  }
}
