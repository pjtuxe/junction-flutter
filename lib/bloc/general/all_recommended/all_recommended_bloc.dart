import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:ijshopflutter/network/api_provider.dart';
import './bloc.dart';

class AllRecommendedBloc
    extends Bloc<AllRecommendedEvent, AllRecommendedState> {
  AllRecommendedBloc() : super(InitialAllRecommendedState());

  @override
  Stream<AllRecommendedState> mapEventToState(
      AllRecommendedEvent event) async* {
    if (event is GetAllRecommended) {
      yield* _getAllRecommended(event.sessionId, event.apiToken);
    }
  }
}

Stream<AllRecommendedState> _getAllRecommended(
    String sessionId, apiToken) async* {
  ApiProvider _apiProvider = ApiProvider();

  yield AllRecommendedWaiting();

  try {
    List<ProductModel> data =
        await _apiProvider.getAllRecommended(sessionId, apiToken);
    yield GetAllRecommendedSuccess(productsData: data);
  } catch (ex) {
    print(ex);
    if (ex.message != 'cancel') {
      yield AllRecommendedError(errorMessage: ex.message.toString());
    }
  }
}
