import 'package:meta/meta.dart';

@immutable
abstract class AllRecommendedEvent {}

class GetAllRecommended extends AllRecommendedEvent {
  final String sessionId;
  final apiToken;
  GetAllRecommended({@required this.sessionId, @required this.apiToken});
}
