import 'package:ijshopflutter/model/general/product_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AllRecommendedState {}

class InitialAllRecommendedState extends AllRecommendedState {}

class AllRecommendedError extends AllRecommendedState {
  final String errorMessage;

  AllRecommendedError({this.errorMessage});
}

class AllRecommendedWaiting extends AllRecommendedState {}

class GetAllRecommendedSuccess extends AllRecommendedState {
  final List<ProductModel> productsData;
  GetAllRecommendedSuccess({@required this.productsData});
}
