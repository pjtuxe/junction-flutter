import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ijshopflutter/config/constants.dart';
import 'package:ijshopflutter/config/global_style.dart';
import 'package:ijshopflutter/model/account/preference_model.dart';
import 'package:ijshopflutter/network/api_provider.dart';
import 'package:ijshopflutter/ui/bottom_navigation_bar.dart';
import 'package:ijshopflutter/ui/home/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class PreferencesPage extends StatefulWidget {
  _PreferencesPageState createState() => _PreferencesPageState();
}

class _PreferencesPageState extends State<PreferencesPage> {
  ApiProvider provider = ApiProvider();
  CancelToken apiToken = CancelToken();

  Map preferenceModel = new Map();
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false,
  // false
  // );

  @override
  void initState() {
    super.initState();

    _asyncMethod();
  }

  _asyncMethod() async {
    var result = await provider.getUser('0fbcd7a4-e3b5-422f-98fb-65e61c22ef94', apiToken);
    this.setState(() {
      this.preferenceModel = result.preferences;
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          elevation: 0,
          title: Text(
            'Preferences',
            style: GlobalStyle.appBarTitle,
          ),
          backgroundColor: Colors.white,
          bottom: PreferredSize(
              child: Container(
                color: Colors.grey[100],
                height: 1.0,
              ),
              preferredSize: Size.fromHeight(1.0)),
        ),
        body:
        ListView(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                top: 20,
                right: 40,
                bottom: 20,
              ),
              child: Text('Do you have any food intolerance? ', style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.bold
              )),
            ),
            SizedBox(
              height: 8,
            ),
            _buildGluten(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildPeanuts(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildTreeNuts(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildCelery(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildMustard(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildEggs(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildMilk(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildSesame(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildFish(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildCrustaceans(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildMolluscs(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildSoya(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildSulphites(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildLupin(),
            Divider(height: 0, color: Colors.grey[400],),
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                top: 20,
                right: 40,
                bottom: 20,
              ),
              child: Text('Which topics are important for you? ', style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.bold
              )),
            ),
            SizedBox(
              height: 8,
            ),
            _buildNoPlastic(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildCrueltyFree(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildNatural(),
            Divider(height: 0, color: Colors.grey[400],),
            _buildVegan(),
            Divider(height: 0, color: Colors.grey[400],),

            FlatButton(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4),
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNavigationBarPage(i: 0)));
              },
              color: PRIMARY_COLOR,
              child: Text(
                'Save',
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
                textAlign: TextAlign.center,
              ),
            )
          ],
        )
    );
  }

  Widget _buildGluten() {
    bool gluten = preferenceModel['gluten'] != null ? preferenceModel['gluten'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Gluten',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: gluten,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['gluten'] = value;
        });
      },
    );
  }

  Widget _buildVegan() {
    bool vegan = preferenceModel['vegan'] != null ? preferenceModel['vegan'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Vegan',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: vegan,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['vegan'] = value;
        });
      },
    );
  }

  Widget _buildNatural() {
    bool natural = preferenceModel['natural'] != null ? preferenceModel['natural'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Natural',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: natural,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['natural'] = value;
        });
      },
    );
  }

  Widget _buildCrueltyFree() {
    bool crueltyFree = preferenceModel['crueltyFree'] != null ? preferenceModel['crueltyFree'] : false;

    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Cruelty Free',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: crueltyFree,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['crueltyFree'] = value;
        });
      },
    );
  }

  Widget _buildNoPlastic() {
    bool noPlastic = preferenceModel['noPlastic'] != null ? preferenceModel['noPlastic'] : false;

    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'No Plastic',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: noPlastic,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['noPlastic'] = value;
        });
      },
    );
  }

  Widget _buildLupin() {
    bool lupin = preferenceModel['lupin'] != null ? preferenceModel['lupin'] : false;

    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Lupin',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: lupin,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['lupin'] = value;
        });
      },
    );
  }

  Widget _buildSulphites() {
    bool sulphites = preferenceModel['sulphites'] != null ? preferenceModel['sulphites'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Sulphites',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: sulphites,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['sulphites'] = value;
        });
      },
    );
  }

  Widget _buildPeanuts() {
    bool peanut = preferenceModel['peanuts'] != null ? preferenceModel['peanuts'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Peanuts',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: peanut,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['peanuts'] = value;
        });
      },
    );
  }

  Widget _buildTreeNuts() {
    bool treeNuts = preferenceModel['treeNuts'] != null ? preferenceModel['treeNuts'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Tree Nuts',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: treeNuts,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['treeNuts'] = value;
        });
      },
    );
  }

  Widget _buildCelery() {
    bool celery = preferenceModel['celery'] != null ? preferenceModel['celery'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Celery',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: celery,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['celery'] = value;
        });
      },
    );
  }

  Widget _buildMustard() {
    bool mustard = preferenceModel['mustard'] != null ? preferenceModel['mustard'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Mustard',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: mustard,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['mustard'] = value;
        });
      },
    );
  }

  Widget _buildEggs() {
    bool eggs = preferenceModel['eggs'] != null ? preferenceModel['eggs'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Eggs',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: eggs,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['eggs'] = value;
        });
      },
    );
  }

  Widget _buildMilk() {
    bool milk = preferenceModel['milk'] != null ? preferenceModel['milk'] : false;

    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Milk',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: milk,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['milk'] = value;
        });
      },
    );
  }

  Widget _buildSesame() {
    bool sesame = preferenceModel['sesame'] != null ? preferenceModel['sesame'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Sesame',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: sesame,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['sesame'] = value;
        });
      },
    );
  }

  Widget _buildFish() {
    bool fish = preferenceModel['fish'] != null ? preferenceModel['fish'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Fish',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: fish,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['fish'] = value;
        });
      },
    );
  }

  Widget _buildCrustaceans() {
    bool crustaceans = preferenceModel['crustaceans'] != null ? preferenceModel['crustaceans'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Crustaceans',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: crustaceans,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['crustaceans'] = value;
        });
      },
    );
  }

  Widget _buildMolluscs() {
    bool molluscs = preferenceModel['molluscs'] != null ? preferenceModel['molluscs'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Molluscs',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: molluscs,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['molluscs'] = value;
        });
      },
    );
  }

  Widget _buildSoya() {
    bool soya = preferenceModel['soya'] != null ? preferenceModel['soya'] : false;
    return SwitchListTile(
      contentPadding: EdgeInsets.only(left: 16, right: 4),
      title: Text(
        'Soya',
        style: TextStyle(fontSize: 15, color: CHARCOAL),
      ),
      value: soya,
      activeColor: PRIMARY_COLOR,
      onChanged: (bool value) async {
        setState(() {
          preferenceModel['soya'] = value;
        });
      },
    );
  }


}