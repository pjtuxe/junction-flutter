/*
This is onboarding page

install plugin in pubspec.yaml
- flutter_statusbarcolor => to change status bar color and navigation status bar color (at the very top of the screen) (https://pub.dev/packages/flutter_statusbarcolor)

Don't forget to add all images and sound used in this pages at the pubspec.yaml
 */

import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:ijshopflutter/library/flutter_overboard/overboard.dart';
import 'package:ijshopflutter/library/flutter_overboard/page_model.dart';
import 'package:ijshopflutter/ui/authentication/signin/signin.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingPage extends StatefulWidget {
  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> with TickerProviderStateMixin, WidgetsBindingObserver {

  // create each page of onBoard here
  final _pageList = [
    PageModel(
        color: Colors.white,
        imageAssetPath: 'assets/images/onboarding/personal_profile.jpg',
        title: 'Personalized Profile',
        body: 'Create your own personalized profile to be informed about allergens, animal testing.',
        doAnimateImage: true),
    PageModel(
        color: Colors.white,
        imageAssetPath: 'assets/images/onboarding/check_gif.gif',
        title: 'Know the difference',
        body: 'Check product origin which is guaranteed by our transparent blockchain pipeline from the filed to your table',
        doAnimateImage: true),
    PageModel(
        color: Colors.white,
        imageAssetPath: 'assets/images/onboarding/notify.png',
        title: 'Never forget refilling',
        body: 'Get notifications from our AI system when you are low on supply at home.',
        doAnimateImage: true),
  ];

  @override
  void initState() {
    // start recording lifecycle
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  // this is the function to do something when the apps is switch
  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    }
    super.didChangeAppLifecycleState(state);
  }

  // if user click finish, you won't see this page again until you clear your data of this apps in your phone setting
  void _finishOnboarding() async {
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    await _pref.setBool('onBoarding', false);
  }

  @override
  void dispose() {
    // stop recording lifecycle
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: OverBoard(
          pages: _pageList,
          showBullets: true,
          finishCallback: () {
            _finishOnboarding();

            // after you click finish, direct to signin page
            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => SigninPage()), (Route<dynamic> route) => false);
          },
        )
    );
  }
}
