/*
This is home page

For this homepage, appBar is created at the bottom after CustomScrollView
we used AutomaticKeepAliveClientMixin to keep the state when moving from 1 navbar to another navbar, so the page is not refresh overtime

include file in reuseable/global_function.dart to call function from GlobalFunction
include file in reuseable/global_widget.dart to call function from GlobalWidget
include file in reuseable/cache_image_network.dart to use cache image network
include file in reuseable/shimmer_loading.dart to use shimmer loading
include file in model/home/category/category_for_you_model.dart to get categoryForYouData
include file in model/home/category/category_model.dart to get categoryData
include file in model/home/coupon_model.dart to get couponData
include file in model/home/flashsale_model.dart to get flashsaleData
include file in model/home/home_banner_model.dart to get homeBannerData
include file in model/home/last_search_model.dart to get lastSearchData
include file in model/home/trending_model.dart to get homeTrendingData
include file in model/home/recomended_product_model.dart to get recomendedProductData

install plugin in pubspec.yaml
- carousel_slider => slider images (https://pub.dev/packages/carousel_slider)
- flutter_statusbarcolor => to change status bar color and navigation status bar color (at the very top of the screen) (https://pub.dev/packages/flutter_statusbarcolor)
- fluttertoast => to show toast (https://pub.dev/packages/fluttertoast)

Don't forget to add all images and sound used in this pages at the pubspec.yaml
 */

import 'dart:async';
import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ijshopflutter/bloc/home/category/category/bloc.dart';
import 'package:ijshopflutter/bloc/home/category/category_for_you/bloc.dart';
import 'package:ijshopflutter/bloc/home/flashsale/bloc.dart';
import 'package:ijshopflutter/bloc/home/home_banner/bloc.dart';
import 'package:ijshopflutter/bloc/home/home_trending/bloc.dart';
import 'package:ijshopflutter/bloc/home/last_search/bloc.dart';
import 'package:ijshopflutter/bloc/general/all_recommended/bloc.dart';
import 'package:ijshopflutter/bloc/home/recomended_product/bloc.dart';
import 'package:ijshopflutter/config/constants.dart';
import 'package:ijshopflutter/config/global_style.dart';
import 'package:ijshopflutter/config/static_variable.dart';
import 'package:ijshopflutter/model/home/category/category_for_you_model.dart';
import 'package:ijshopflutter/model/home/category/category_model.dart';
import 'package:ijshopflutter/model/home/flashsale_model.dart';
import 'package:ijshopflutter/model/home/home_banner_model.dart';
import 'package:ijshopflutter/model/home/last_search_model.dart';
import 'package:ijshopflutter/model/home/trending_model.dart';
import 'package:ijshopflutter/model/home/recomended_product_model.dart';
import 'package:ijshopflutter/ui/general/chat_us.dart';
import 'package:ijshopflutter/ui/home/coupon.dart';
import 'package:ijshopflutter/ui/home/flashsale.dart';
import 'package:ijshopflutter/ui/home/last_search.dart';
import 'package:ijshopflutter/ui/general/product_detail/product_detail.dart';
import 'package:ijshopflutter/ui/general/notification.dart';
import 'package:ijshopflutter/ui/home/category/product_category.dart';
import 'package:ijshopflutter/ui/home/search/search_product.dart';
import 'package:ijshopflutter/ui/reuseable/app_localizations.dart';
import 'package:ijshopflutter/ui/reuseable/cache_image_network.dart';
import 'package:ijshopflutter/ui/home/search/search.dart';
import 'package:ijshopflutter/ui/reuseable/global_function.dart';
import 'package:ijshopflutter/ui/reuseable/global_widget.dart';
import 'package:ijshopflutter/ui/reuseable/shimmer_loading.dart';
import 'package:ijshopflutter/bloc/home/category/category_trending_product/bloc.dart';
import 'package:ijshopflutter/bloc/general/products/bloc.dart';
import 'package:ijshopflutter/model/home/category/category_trending_product_model.dart';
import 'package:ijshopflutter/model/general/product_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  // initialize global function, global widget and shimmer loading
  final _globalFunction = GlobalFunction();
  final _globalWidget = GlobalWidget();
  final _shimmerLoading = ShimmerLoading();

  List<FlashsaleModel> flashsaleData = List();
  List<LastSearchModel> lastSearchData = List();
  List<HomeTrendingModel> homeTrendingData = List();
  List<CategoryForYouModel> categoryForYouData = List();
  List<CategoryModel> categoryData = List();
  List<HomeBannerModel> homeBannerData = [
    HomeBannerModel.fromJson(
        {
          'id' : 1,
          'image' : 'http://junction-api.pjtuxe.com/static/aIl_board.png',
        },
     ),
    HomeBannerModel.fromJson(
      {
        'id' : 2,
        'image' : 'http://junction-api.pjtuxe.com/static/blockchain.png',
      },
    ),
    HomeBannerModel.fromJson(
      {
        'id' : 3,
        'image' : 'http://junction-api.pjtuxe.com/static/personal.png',
      },
    )
  ];

  HomeBannerBloc _homeBannerBloc;
  bool _lastDataHomeBanner = false;

  FlashsaleBloc _flashsaleBloc;
  bool _lastDataFlashsale = false;

  LastSearchBloc _lastSearchBloc;
  bool _lastDataSeach = false;

  HomeTrendingBloc _homeTrendingBloc;
  bool _lastDataTrending = false;

  CategoryForYouBloc _categoryForYouBloc;
  bool _lastDataCategoryForYou = false;

  CategoryBloc _categoryBloc;
  bool _lastDataCategory = false;

  List<ProductModel> productsData = List();
  ProductsBloc _productsBloc;

  List<ProductModel> allRecommendedData = List();
  AllRecommendedBloc _allRecommendedBloc;

  int _apiPageRecomended = 0;
  bool _lastDataRecomended = false;
  bool _processApiRecomended = false;

  CancelToken apiToken = CancelToken(); // used to cancel fetch data from API

  int _currentImageSlider = 0;

  ScrollController _scrollController;
  Color _topIconColor = Colors.white;
  Color _topSearchColor = Colors.white;
  AnimationController _topColorAnimationController;
  Animation _appBarColor;

  Timer _flashsaleTimer;
  int _flashsaleSecond;

  List<CategoryTrendingProductModel> categoryTrendingProductData = List();
  CategoryTrendingProductBloc _categoryTrendingProductBloc;
  bool _lastDataCategoryTrendingProduct = false;

  void _startFlashsaleTimer() {
    const period = const Duration(seconds: 1);
    _flashsaleTimer = Timer.periodic(period, (timer) {
      setState(() {
        _flashsaleSecond--;
      });
      if (_flashsaleSecond == 0) {
        _cancelFlashsaleTimer();
        Fluttertoast.showToast(
            msg: AppLocalizations.of(context).translate('flash_sale_is_over'),
            toastLength: Toast.LENGTH_LONG);
      }
    });
  }

  void _cancelFlashsaleTimer() {
    if (_flashsaleTimer != null) {
      _flashsaleTimer.cancel();
      _flashsaleTimer = null;
    }
  }

  // keep the state to do not refresh when switch navbar
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    // get data when initState
    _homeBannerBloc = BlocProvider.of<HomeBannerBloc>(context);
    _homeBannerBloc
        .add(GetHomeBanner(sessionId: SESSION_ID, apiToken: apiToken));

    _flashsaleBloc = BlocProvider.of<FlashsaleBloc>(context);
    _flashsaleBloc.add(GetFlashsaleHome(
        sessionId: SESSION_ID, skip: '0', limit: '8', apiToken: apiToken));

    _lastSearchBloc = BlocProvider.of<LastSearchBloc>(context);
    _lastSearchBloc.add(GetLastSearchHome(
        sessionId: SESSION_ID, skip: '0', limit: '8', apiToken: apiToken));

    _homeTrendingBloc = BlocProvider.of<HomeTrendingBloc>(context);
    _homeTrendingBloc.add(GetHomeTrending(
        sessionId: SESSION_ID, skip: '0', limit: '8', apiToken: apiToken));

    _categoryForYouBloc = BlocProvider.of<CategoryForYouBloc>(context);
    _categoryForYouBloc
        .add(GetCategoryForYou(sessionId: SESSION_ID, apiToken: apiToken));

    _categoryBloc = BlocProvider.of<CategoryBloc>(context);
    _categoryBloc.add(GetCategory(sessionId: SESSION_ID, apiToken: apiToken));

    _allRecommendedBloc = BlocProvider.of<AllRecommendedBloc>(context);
    _allRecommendedBloc
        .add(GetAllRecommended(sessionId: SESSION_ID, apiToken: apiToken));

    _categoryTrendingProductBloc =
        BlocProvider.of<CategoryTrendingProductBloc>(context);
    _categoryTrendingProductBloc.add(GetCategoryTrendingProduct(
        sessionId: SESSION_ID,
        categoryId: 0,
        skip: '0',
        limit: '8',
        apiToken: apiToken));

    _productsBloc = BlocProvider.of<ProductsBloc>(context);
    _productsBloc.add(GetProducts(sessionId: SESSION_ID, apiToken: apiToken));

    setupAnimateAppbar();

    // set how many times left for flashsale
    var timeNow = DateTime.now();

    // 8000 second = 2 hours 13 minutes 20 second for flashsale timer
    var flashsaleTime =
        timeNow.add(Duration(seconds: 8000)).difference(timeNow);
    _flashsaleSecond = flashsaleTime.inSeconds;
    _startFlashsaleTimer();

    super.initState();
  }

  @override
  void dispose() {
    apiToken.cancel("cancelled"); // cancel fetch data from API

    _scrollController.dispose();
    _topColorAnimationController.dispose();

    _cancelFlashsaleTimer();
    super.dispose();
  }

  void setupAnimateAppbar() {
    // use this function and paramater to animate top bar
    _topColorAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 0));
    _appBarColor = ColorTween(begin: Colors.transparent, end: Colors.white)
        .animate(_topColorAnimationController);
    _scrollController = ScrollController()
      ..addListener(() {
        _topColorAnimationController.animateTo(_scrollController.offset / 120);
        // if scroll for above 150, then change app bar color to white, search button to dark, and top icon color to dark
        // if scroll for below 150, then change app bar color to transparent, search button to white and top icon color to light
        if (_scrollController.hasClients &&
            _scrollController.offset > (150 - kToolbarHeight)) {
          if (_topIconColor != BLACK_GREY) {
            StaticVariable.homeIsScroll = true;
            StaticVariable.useWhiteStatusBarForeground = false;
            FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
            _topIconColor = BLACK_GREY;
            _topSearchColor = Colors.grey[100];
          }
        } else {
          if (_topIconColor != Colors.white) {
            StaticVariable.homeIsScroll = false;
            StaticVariable.useWhiteStatusBarForeground = true;
            FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
            _topIconColor = Colors.white;
            _topSearchColor = Colors.white;
          }
        }

        // this function used to fetch data from API if we scroll to the bottom of the page
        double maxScroll = _scrollController.position.maxScrollExtent;
        double currentScroll = _scrollController.position.pixels;

        /*
      only for home.dart, flashsale.dart and product_category.dart
      you need to check if the skip == 0, use timer
     */
        if (_apiPageRecomended == 0) {
          Timer(Duration(milliseconds: 3000), () {
            if (currentScroll == maxScroll) {
              if (_lastDataRecomended == false && !_processApiRecomended) {
                _productsBloc.add(
                    GetProducts(sessionId: SESSION_ID, apiToken: apiToken));
                _processApiRecomended = true;
              }
            }
          });
        } else {
          if (currentScroll == maxScroll) {
            if (_lastDataRecomended == false && !_processApiRecomended) {
              _productsBloc
                  .add(GetProducts(sessionId: SESSION_ID, apiToken: apiToken));
              _processApiRecomended = true;
            }
          }
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    // if we used AutomaticKeepAliveClientMixin, we must call super.build(context);
    super.build(context);

    final double boxImageSize = (MediaQuery.of(context).size.width / 3);
    final double categoryForYouHeightShort = boxImageSize;
    final double categoryForYouHeightLong = (boxImageSize * 2);

    return Scaffold(
      body: MultiBlocListener(
        listeners: [
          BlocListener<HomeBannerBloc, HomeBannerState>(
            listener: (context, state) {
              if (state is HomeBannerError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetHomeBannerSuccess) {
                if (state.homeBannerData.length == 0) {
                  _lastDataHomeBanner = true;
                } else {
                  print(state.homeBannerData);
                  // homeBannerData.addAll(state.homeBannerData);
                }
              }
            },
          ),
          BlocListener<FlashsaleBloc, FlashsaleState>(
            listener: (context, state) {
              if (state is FlashsaleHomeError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetFlashsaleHomeSuccess) {
                if (state.flashsaleData.length == 0) {
                  _lastDataFlashsale = true;
                } else {
                  flashsaleData.addAll(state.flashsaleData);
                }
              }
            },
          ),
          BlocListener<LastSearchBloc, LastSearchState>(
            listener: (context, state) {
              if (state is LastSearchHomeError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetLastSearchHomeSuccess) {
                if (state.lastSearchData.length == 0) {
                  _lastDataSeach = true;
                } else {
                  lastSearchData.addAll(state.lastSearchData);
                }
              }
            },
          ),
          BlocListener<AllRecommendedBloc, AllRecommendedState>(
              listener: (context, state) {
            if (state is AllRecommendedError) {
              _globalFunction.showToast(
                  type: 'error', message: state.errorMessage);
            }

            if (state is GetAllRecommendedSuccess) {
              if (state.productsData.length == 0) {
              } else {
                allRecommendedData.addAll(state.productsData);
              }
            }
          }),
          BlocListener<HomeTrendingBloc, HomeTrendingState>(
            listener: (context, state) {
              if (state is HomeTrendingError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetHomeTrendingSuccess) {
                homeTrendingData.addAll(state.homeTrendingData);
              }
            },
          ),
          BlocListener<CategoryForYouBloc, CategoryForYouState>(
            listener: (context, state) {
              if (state is CategoryForYouError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetCategoryForYouSuccess) {
                if (state.categoryForYouData.length == 0) {
                  _lastDataCategoryForYou = true;
                } else {
                  categoryForYouData.addAll(state.categoryForYouData);
                }
              }
            },
          ),
          BlocListener<ProductsBloc, ProductsState>(
            listener: (context, state) {
              if (state is ProductsError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetProductsSuccess) {
                if (state.productsData.length == 0) {
                  _lastDataRecomended = true;
                } else {
                  _apiPageRecomended += LIMIT_PAGE;
                  productsData.addAll(state.productsData);
                }
                _processApiRecomended = false;
              }
            },
          ),
          BlocListener<CategoryBloc, CategoryState>(
            listener: (context, state) {
              if (state is CategoryError) {
                _globalFunction.showToast(
                    type: 'error', message: state.errorMessage);
              }
              if (state is GetCategorySuccess) {
                if (state.categoryData.length == 0) {
                  _lastDataCategory = true;
                } else {
                  categoryData.addAll(state.categoryData);
                }
              }
            },
          ),
        ],
        child: RefreshIndicator(
          onRefresh: () {},
          child: Stack(
            children: [
              CustomScrollView(
                controller: _scrollController,
                slivers: [
                  SliverList(
                      delegate: SliverChildListDelegate([
                    _createHomeBannerSlider(),
                    // _createCoupon(),
                    // _createGridCategory(),
                    Container(
                      margin: EdgeInsets.only(top: 20, left: 16, right: 16),
                      child: Text(
                          AppLocalizations.of(context)
                              .translate('just_for_you'),
                          style: GlobalStyle.sectionTitle),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 8),
                        height: boxImageSize *
                            GlobalStyle.horizontalProductHeightMultiplication,
                        child: BlocBuilder<AllRecommendedBloc,
                            AllRecommendedState>(
                          builder: (context, state) {
                            if (state is AllRecommendedError) {
                              return Container(
                                  child: Center(
                                      child: Text(ERROR_OCCURED,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: BLACK_GREY))));
                            } else {
                              if (allRecommendedData.length == 0) {
                                return _shimmerLoading
                                    .buildShimmerHorizontalProduct(
                                        boxImageSize);
                              } else {
                                return ListView.builder(
                                  padding: EdgeInsets.only(left: 12, right: 12),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: allRecommendedData.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return _buildTrendingProductCardNew(
                                        index, boxImageSize);
                                  },
                                );
                              }
                            }
                          },
                        )),
                    // Container(
                    //   margin: EdgeInsets.only(top: 30, left: 16, right: 16),
                    //   child: Text(
                    //       AppLocalizations.of(context)
                    //           .translate('category_for_you'),
                    //       style: GlobalStyle.sectionTitle),
                    // ),
                    // _createCategoryForYou(boxImageSize,
                    //     categoryForYouHeightShort, categoryForYouHeightLong),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 16, right: 16),
                      child:
                          Text('All Products', style: GlobalStyle.sectionTitle),
                    ),
                    BlocBuilder<ProductsBloc, ProductsState>(
                      builder: (context, state) {
                        if (state is ProductsError) {
                          return Container(
                              child: Center(
                                  child: Text(ERROR_OCCURED,
                                      style: TextStyle(
                                          fontSize: 14, color: BLACK_GREY))));
                        } else {
                          if (_lastDataRecomended && _apiPageRecomended == 0) {
                            return Container(
                                child: Text(
                                    AppLocalizations.of(context)
                                        .translate('no_recomended_product'),
                                    style: TextStyle(
                                        fontSize: 14, color: BLACK_GREY)));
                          } else {
                            if (productsData.length == 0) {
                              return _shimmerLoading.buildShimmerProduct(
                                  ((MediaQuery.of(context).size.width) - 24) /
                                          2 -
                                      12);
                            } else {
                              return CustomScrollView(
                                  shrinkWrap: true,
                                  primary: false,
                                  slivers: <Widget>[
                                    SliverPadding(
                                      padding:
                                          EdgeInsets.fromLTRB(12, 8, 12, 8),
                                      sliver: SliverGrid(
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          mainAxisSpacing: 8,
                                          crossAxisSpacing: 8,
                                          childAspectRatio:
                                              GlobalStyle.gridDelegateRatio,
                                        ),
                                        delegate: SliverChildBuilderDelegate(
                                          (BuildContext context, int index) {
                                            return _buildRecomendedProductCard(
                                                index);
                                          },
                                          childCount: productsData.length,
                                        ),
                                      ),
                                    ),
                                    SliverToBoxAdapter(
                                      child: (_apiPageRecomended ==
                                                  LIMIT_PAGE &&
                                              productsData.length < LIMIT_PAGE)
                                          ? Wrap()
                                          : _globalWidget
                                              .buildProgressIndicator(
                                                  _lastDataRecomended),
                                    ),
                                  ]);
                            }
                          }
                        }
                      },
                    ),
                  ])),
                ],
              ),
              // Create AppBar with Animation
              Container(
                height: 80,
                child: AnimatedBuilder(
                  animation: _topColorAnimationController,
                  builder: (context, child) => AppBar(
                    backgroundColor: _appBarColor.value,
                    elevation: 0,
                    title: Container(
                      child: SizedBox(
                          width: double.maxFinite,
                          child: RaisedButton(
                            elevation: 0,
                            splashColor: _topSearchColor,
                            highlightColor: _topSearchColor,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5),
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SearchPage()));
                            },
                            color: _topSearchColor,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.search,
                                  color: Colors.grey[500],
                                  size: 18,
                                ),
                                SizedBox(width: 8),
                                Text(
                                  AppLocalizations.of(context)
                                      .translate('search_product'),
                                  style: TextStyle(
                                      color: Colors.grey[500],
                                      fontWeight: FontWeight.normal),
                                )
                              ],
                            ),
                          )),
                    ),
                    actions: [
                      // GestureDetector(
                      // onTap: () {
                      //   Navigator.push(
                      //       context,
                      //       MaterialPageRoute(
                      //           builder: (context) => ChatUsPage()));
                      // },
                      // child: Icon(Icons.email, color: _topIconColor)),
                      IconButton(
                          icon: _globalWidget.customNotifIcon(4, _topIconColor),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NotificationPage()));
                          }),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future refreshData() async {
    setState(() {
      _apiPageRecomended = 0;
      homeBannerData.clear();
      flashsaleData.clear();
      lastSearchData.clear();
      homeTrendingData.clear();
      categoryForYouData.clear();
      categoryData.clear();
      _homeBannerBloc
          .add(GetHomeBanner(sessionId: SESSION_ID, apiToken: apiToken));
      _flashsaleBloc.add(GetFlashsaleHome(
          sessionId: SESSION_ID, skip: '0', limit: '8', apiToken: apiToken));
      _lastSearchBloc.add(GetLastSearchHome(
          sessionId: SESSION_ID, skip: '0', limit: '8', apiToken: apiToken));
      _homeTrendingBloc.add(GetHomeTrending(
          sessionId: SESSION_ID, skip: '0', limit: '8', apiToken: apiToken));
      _categoryForYouBloc
          .add(GetCategoryForYou(sessionId: SESSION_ID, apiToken: apiToken));
      _categoryBloc.add(GetCategory(sessionId: SESSION_ID, apiToken: apiToken));
    });
  }

  Widget _createHomeBannerSlider() {
    double homeBannerWidth = MediaQuery.of(context).size.width;
    double homeBannerHeight = MediaQuery.of(context).size.width * 6 / 8;

    return BlocBuilder<HomeBannerBloc, HomeBannerState>(
      builder: (context, state) {
        if (state is HomeBannerError) {
          return Container(
              width: homeBannerWidth,
              height: homeBannerHeight,
              child: Center(
                  child: Text(ERROR_OCCURED,
                      style: TextStyle(fontSize: 14, color: BLACK_GREY))));
        } else {
          if (_lastDataHomeBanner) {
            return Container(
                width: homeBannerWidth,
                height: homeBannerHeight,
                child: Center(
                    child: Text(
                        AppLocalizations.of(context)
                            .translate('no_home_banner_data'),
                        style: TextStyle(fontSize: 14, color: BLACK_GREY))));
          } else {
            if (homeBannerData.length == 0) {
              return _shimmerLoading.buildShimmerHomeBanner(
                  homeBannerWidth, homeBannerHeight);
            } else {
              return Column(
                children: [
                  CarouselSlider(
                    items: homeBannerData
                        .map((item) => Container(
                              child: buildCacheNetworkImage(
                                  width: 0, height: 0, url: item.image),
                            ))
                        .toList(),
                    options: CarouselOptions(
                        aspectRatio: 8 / 6,
                        viewportFraction: 1.0,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 6),
                        autoPlayAnimationDuration: Duration(milliseconds: 300),
                        enlargeCenterPage: false,
                        onPageChanged: (index, reason) {
                          setState(() {
                            _currentImageSlider = index;
                          });
                        }),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: homeBannerData.map((item) {
                      int index = homeBannerData.indexOf(item);
                      return Container(
                        width: 8.0,
                        height: 8.0,
                        margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 2.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _currentImageSlider == index
                              ? PRIMARY_COLOR
                              : Colors.grey[300],
                        ),
                      );
                    }).toList(),
                  ),
                ],
              );
            }
          }
        }
      },
    );
  }

  Widget _buildTrendingProductCardNew(index, boxImageSize) {
    return Container(
      width: boxImageSize + 10,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 2,
        color: Colors.white,
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductDetailPage(
                        fromWhite: null,
                        productId: allRecommendedData[index].id,
                        name: allRecommendedData[index].title,
                        image: allRecommendedData[index].imageUrl,
                        price: allRecommendedData[index].price,
                        sale: 44)));
          },
          child: Column(
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  child: buildCacheNetworkImage(
                      width: boxImageSize + 10,
                      height: boxImageSize + 10,
                      url: allRecommendedData[index].imageUrl)),
              Container(
                margin: EdgeInsets.fromLTRB(8, 8, 8, 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      allRecommendedData[index].title,
                      style: GlobalStyle.productName,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Text(
                          '\$ ' +
                              _globalFunction.removeDecimalZeroFormat(
                                  allRecommendedData[index].price),
                          style: GlobalStyle.productPrice),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Row(
                        children: [
                          _globalWidget.createRatingBar(5),
                          Text('(500)', style: GlobalStyle.productTotalReview)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRecomendedProductCard(index) {
    if (productsData == null) {
      return Container();
    }

    Random random = new Random();
    final double boxImageSize =
        ((MediaQuery.of(context).size.width) - 24) / 2 - 12;
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 2,
        color: Colors.white,
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductDetailPage(
                        productId: productsData[index].id,
                        fromWhite: null,
                        name: productsData[index].title,
                        image: productsData[index].imageUrl,
                        price: productsData[index].price)));
          },
          child: Column(
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  child: buildCacheNetworkImage(
                      width: boxImageSize,
                      height: boxImageSize,
                      url: productsData[index].imageUrl)),
              Container(
                margin: EdgeInsets.fromLTRB(8, 8, 8, 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      productsData[index].title,
                      style: GlobalStyle.productName,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              '\$ ' +
                                  _globalFunction.removeDecimalZeroFormat(
                                      productsData[index].price),
                              style: GlobalStyle.productPrice)
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Row(
                        children: [
                          _globalWidget.createRatingBar(5),
                          Text('(1500)', style: GlobalStyle.productTotalReview)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
