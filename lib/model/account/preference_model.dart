import 'dart:convert';

class PreferenceModel {
  bool gluten = false;
  bool peanuts = false;
  bool treeNuts = false;
  bool celery = false;
  bool mustard = false;
  bool eggs = false;
  bool milk = false;
  bool sesame = false;
  bool fish = false;
  bool crustaceans = false;
  bool molluscs = false;
  bool soya = false;
  bool sulphites = false;
  bool lupin = false;
  bool noPlastic = false;
  bool crueltyFree = false;
  bool natural = false;
  bool vegan = false;

  PreferenceModel(
      this.gluten,
      this.peanuts,
      this.treeNuts,
      this.celery,
      this.mustard,
      this.eggs,
      this.milk,
      this.sesame,
      this.fish,
      this.crustaceans,
      this.molluscs,
      this.soya,
      this.sulphites,
      this.lupin,
      this.noPlastic,
      this.crueltyFree,
      this.natural,
      this.vegan);

  PreferenceModel.fromJson(Map<String, dynamic> json) {
    gluten = json['gluten'];
    peanuts = json['peanuts'];
    treeNuts = json['tree_nuts'];
    celery = json['celery'];
    mustard = json['mustard'];
    eggs = json['eggs'];
    milk = json['milk'];
    sesame = json['sesame'];
    fish = json['fish'];
    crustaceans = json['crustaceans'];
    molluscs = json['molluscs'];
    soya = json['soya'];
    sulphites = json['sulphites'];
    lupin = json['lupin'];
    noPlastic = json['no_plastic'];
    crueltyFree = json['cruelty_free'];
    natural = json['natural'];
    vegan = json['vegan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['gluten'] = this.gluten;
    data['peanuts'] = this.peanuts;
    data['tree_nuts'] = this.treeNuts;
    data['celery'] = this.celery;
    data['mustard'] = this.mustard;
    data['eggs'] = this.eggs;
    data['milk'] = this.milk;
    data['sesame'] = this.sesame;
    data['fish'] = this.fish;
    data['crustaceans'] = this.crustaceans;
    data['molluscs'] = this.molluscs;
    data['soya'] = this.soya;
    data['sulphites'] = this.sulphites;
    data['lupin'] = this.lupin;
    data['no_plastic'] = this.noPlastic;
    data['cruelty_free'] = this.crueltyFree;
    data['natural'] = this.natural;
    data['vegan'] = this.vegan;
    return data;
  }
}
