import 'dart:convert';

import 'package:ijshopflutter/model/account/preference_model.dart';

class UserModel {
  String email;
  String userId;
  Map<String, dynamic> preferences;


  UserModel(
      {this.email,
      this.userId,
      this.preferences});

  UserModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    userId = json['userId'];
    preferences = json['preferences'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['userId'] = this.userId;
    data['preferences'] = this.preferences;
    return data;
  }
}
