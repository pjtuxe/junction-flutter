class ShoppingCartModel {
  int id;
  String name;
  String image;
  double price;
  int qty;

  ShoppingCartModel({this.id, this.image, this.name, this.price, this.qty});

  void setQty(int i) {
    if(i<1){
      qty = 1;
    } else {
      qty = i;
    }
  }

  ShoppingCartModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    price = json['price'].toDouble();
    qty = json['qty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    data['price'] = this.price;
    data['qty'] = this.qty;
    return data;
  }
}