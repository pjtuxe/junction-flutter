class FlashsaleModel {
  int id;
  String name;
  double price;
  String image;
  int discount;
  double countItem;
  int sale;

  FlashsaleModel({this.id, this.name, this.price, this.image, this.discount, this.countItem, this.sale});

  FlashsaleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'].toDouble();
    image = json['image'];
    discount = json['discount'];
    countItem = json['countItem'].toDouble();
    sale = json['sale'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['image'] = this.image;
    data['discount'] = this.discount;
    data['countItem'] = this.countItem;
    data['sale'] = this.sale;
    return data;
  }
}