import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ijshopflutter/config/constants.dart';

class CategoryModel {
  int id;
  String name;
  String image;

  CategoryModel({this.id, this.name, this.image});

  Icon getIcon(String name){
    IconData iconData = Icons.local_mall;
    Color iconColor = Colors.orange;

    if(name == 'Fashion'){
      iconData = Icons.local_mall;
      iconColor = Colors.orange;
    } else if(name == 'Smartphone & Tablets'){
      iconData = Icons.tablet_android;
      iconColor = CHARCOAL;
    } else if(name == 'Electronic'){
      iconData = Icons.tablet_android;
      iconColor = Colors.blueGrey[300];
    } else if(name == 'Otomotif'){
      iconData = Icons.directions_car;
      iconColor = Color(0xFF0a4349);
    } else if(name == 'Sport'){
      iconData = Icons.directions_bike;
      iconColor = SOFT_BLUE;
    } else if(name == 'Food'){
      iconData = Icons.fastfood;
      iconColor = Colors.red[700];
    } else if(name == 'Voucher\nGame'){
      iconData = Icons.videogame_asset;
      iconColor = Colors.blueGrey[600];
    } else if(name == 'Health & Care'){
      iconData = Icons.local_hospital;
      iconColor = Colors.red;
    }

    return Icon(iconData, size: 40, color: iconColor);
  }

  CategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}