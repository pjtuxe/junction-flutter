class ProductModel {
  String id;
  String title;
  String description;
  String kind;
  String availability;
  String brand;
  String color;
  String gender;
  double price;
  String productType;
  String sizes;
  Object ingredients;
  Object preferences;
  String imageUrl;

  ProductModel(
      {this.id,
      this.title,
      this.description,
      this.kind,
      this.availability,
      this.brand,
      this.color,
      this.gender,
      this.price,
      this.productType,
      this.sizes,
      this.ingredients,
      this.preferences,
      this.imageUrl});

  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    title = json['title'];
    description = json['description'];
    kind = json['kind'];
    availability = json['availability'];
    brand = json['brand'];
    color = json['color'];
    gender = json['gender'];
    price = json['price'].toDouble();
    productType = json['productType'];
    sizes = json['sizes'];
    ingredients = json['ingredients'];
    preferences = json['preferences'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    data['kind'] = this.kind;
    data['availability'] = this.availability;
    data['brand'] = this.brand;
    data['color'] = this.color;
    data['gender'] = this.gender;
    data['price'] = this.price;
    data['productType'] = this.productType;
    data['sizes'] = this.sizes;
    data['ingredients'] = this.ingredients;
    data['preferences'] = this.preferences;
    data['image_url'] = this.imageUrl;
    return data;
  }
}
